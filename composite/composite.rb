class Equipment

	attr_reader :name

	def initialize(name)
		@name = name
	end

	def power
	end

	def net_price
	end

	def dicountPrice
	end

	def add
	end

	def remove
	end

	def create_iterator
	end

end

class FloppyDisk < Equipment # листовой класс
	def power
	end

	def net_price
	end

	def dicountPrice
	end
end

class CompositeEquipment < Equipment  # базовый класс для оборудования, содержащего другое оборудование

	@equipment = [] # array of Equipment

	def power
	end

	def net_price
		@equipment.inject(0) { |sum, item| sum += item.net_price}
	end

	def dicountPrice
	end

	def add
		# add to equipment
	end

	def remove
		# remove from equipment
	end

	def create_iterator
	end
end

class Chassis < CompositeEquipment
	def power
	end

	def net_price
	end

	def dicountPrice
	end
end

class Cabinet < Equipment
	def power
	end

	def net_price
	end

	def dicountPrice
	end

	def add
		# add to equipment
	end

	def remove
		# remove from equipment
	end

	def create_iterator
	end
end

class Bus < Equipment
	def power
	end

	def net_price
	end

	def dicountPrice
	end
	
	def add
		# add to equipment
	end

	def remove
		# remove from equipment
	end

	def create_iterator
	end

end