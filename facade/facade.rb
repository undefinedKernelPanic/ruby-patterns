class Scanner

	def initialize(istream)
		@inputStream = istream
	end

	def scan

	end

end

class Parser
	def parse(scanner, program_node_builder)
	end
end

class ProgramNodeBuilder
	def initialize
		@node = nil
	end

	def new_variable(var_name)

	end

	def new_assignment(program_node_variable, program_node_expression)

	end

	def new_return_statement(program_node_value)

	end

	def new_condition(program_node_condition, program_node_true_part, program_node_false_part)
	end

	def get_root_node
	end
end

class ProgramNode
	# манипулирование узлом программы
	def get_source_position(line, index)
	end

	# манипулирование потомками
	def add(program_node)
	end

	def remove(program_node)
	end




	def traverse(code_generator)
	end
end

class CodeGenerator

	def initialize(output)
		@output = output
	end

	def visit(statement_expression_node)
	end
end

class Compiler # Facade
	def compile(input, output)
		scanner = Scanner.new input
		builder = ProgramNodeBuilder.new
		parser = Parser.new

		parser.parse scanner, builder

		generator = RISCCodeGenerator.new output
		parse_tree = builder.get_root_node
		parse_tree.traverse generator
	end


end