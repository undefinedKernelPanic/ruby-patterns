class Shape
	
	def bounding_box(bot_left, right_top)
	end

	def create_manipulator
	end
end

class TextView
	def get_origin(x, y)
	end

	def get_extent(w, h)
	end

	def is_empty
	end
end

class TextShape < Shape           # адаптер между Shape and TextView
	def initialize(text_view)    # TextShape.new TextView.new
		@text_view = text_view
	end

	def bounding_box(bot_left, right_top) # эта операция преобразует интерфейс TextView к Shape
		bottom, left = @text_view.get_origin
		width, height = @text_view.get_extent

		bottom_left = bottom, left
		top_right = [bottom + height, left + width]
	end

	def is_empty # демонстрирование прямой переадрисации запросов, общих для обоих классов
		@text_view.is_empty
	end

	def create_manipulator
		TextManipulator.new
	end
end