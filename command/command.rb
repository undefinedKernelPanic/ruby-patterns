class Command # абстракный класс, в котором объявлен интерфейс для выполнения операций

	def execute
		raise NonEmplementedError
	end

end

class OpenCommand < Command # определяют пару получатель-действие

	def initialize(app)
		# @response = 
		@application = app # получатель, у него есть инфа как выполнить запрос
	end

	def execute
		name = ask_user
		doc = Document.new name
		@application.add doc
		doc.open
	end

	protected

	def ask_user
		'hey'
	end	
end


class PasteCommand < Command # определяют пару получатель-действие

	def initialize(doc)
		@document = doc # получатель, у него есть инфа как выполнить запрос
	end

	def execute
		@document.paste
	end
end