require 'singleton'

class TCPConnection # предостваляет интерфейс для передачи данных и обрабатывает запросы на изменение состояния

	attr :state # экземпляр класса TCPState 

	def initialize
		@state = TCPCLosed.instance
	end

	def change_state(state)
		@state = state
	end

	def active_open
		@state.active_open self
	end

	def passive_open
		@state.passive_open self
	end

	def close
		@state.close self
	end

	def send
	end

	def acknowledge
		@state.acknowledge self
	end

	def synchronize
		@state.synchronize self
	end

	def process_octet(tcp_octet_stream)
	end
end


class TCPState # дублирует интерфейс изменения состояния

	include Singleton

	def transmit(tcp_connection, tcp_octet_stream)
	end

	def active_open(tcp_connection)
	end

	def passive_open(tcp_connection)
	end

	def close(tcp_connection)
	end

	def send(tcp_connection)
	end

	def acknowledge(tcp_connection)
	end

	def synchronize
	end

	protected

	def change_state(tcp_connection, tcp_state)
		tcp_connection.change_state tcp_state
	end
end

class TCPEctablished < TCPState
	def transmit(tcp_connection, tcp_octet_stream)
		tcp_connection.process_octet tcp_octet_stream
	end

	def close(tcp_connection)
		change_state tcp_connection, TCPListen.instance
	end
end

class TCPListen < TCPState
	def send(tcp_connection)
		change_state tcp_connection, TCPEctablished.instance
	end
end

class TCPClosed < TCPState
	def active_open(tcp_connection)
		#some actions
		change_state tcp_connection, TCPEctablished.instance
	end

	def passive_open(tcp_connection)
		#some actions
		change_state tcp_connection, TCPEctablished.instance
	end
end