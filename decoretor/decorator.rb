class Window
	def initialize
		@content = []
	end


	def add_content(item)
		@content.push item
	end

	def print_result
		@content.each { |i| p i.print }
	end
end


class VisualComponent

	def print
		'0'
	end

end


class TextView < VisualComponent

end

class Decorator < VisualComponent

	def initialize(visual_component)
		@visual_component = visual_component #декорируемый объект
	end

	def print
		@visual_component.print
	end

end

class PlusDecorator < VisualComponent

	def initialize(visual_component, count)
		@visual_component = visual_component #декорируемый объект
		@count = count
	end

	def print
		"#{print_plus}#{@visual_component.print}#{print_plus}"
	end

	private

	def print_plus
		'+' * @count
	end
end

class EqualDecorator < VisualComponent

	def initialize(visual_component)
		@visual_component = visual_component #декорируемый объект
	end

	def print
		"#{print_equal}#{@visual_component.print}#{print_equal}"
	end

	private

	def print_equal
		'='
	end
end

#--------------------------------------------

text_view = TextView.new
w = Window.new

w.add_content(EqualDecorator.new PlusDecorator.new(text_view, 3))
w.add_content text_view
w.print_result