class MapSite
	def enter
		0
	end
end

class Room < MapSite
	direction = [:north, :south, :eash, :west]

	def inicialize(room_number)
		@room_number = room_number
	end

	def set_side(direction, map_site)
	end
end

class Maze
	def add_room(room)

	end

	def room_n(room_number)
	end
end

class Wall < MapSite

end

class Door < MapSite
	def inicialize(room1, room2)
	end
end

class MazeGame

	def create_maze
		a_maze = make_maze

		r1 = make_room 1
		r2 = make_room 2
		the_door = make_door r1, r2

		a_maze.add_room r1
		a_maze.add_room r2

		r1.set_side :north, make_wall

		a_maze
	end

	# фабричные методы

	def make_maze
		Maze.new
	end

	def make_room(room_number)
		Room.new room_number
	end

	def make_wall
		Wall.new
	end

	def make_door(r1, r2)

	end

end

class BombedMazeGame < MazeGame
	def make_wall
		BombedWall.new
	end

	def make_room(room_number)
		RoomWithABomb.new room_number
	end
end


class EnchantedMazeGame < MazeGame
	def make_wall
		BombedWall.new
	end

	def make_room(room_number)
		EnchantedRoom.mew room_number, -> { cast_spell }
	end

	def make_door(r1, r2)
		DoorNeedingSpell r1, r2
	end

	protected

	def cast_spell
	end
end