require_relative 'shape_imp'

class Shape

	def initialize(system)
		@imp = get_system_imp system
	end

	def draw
		raise NotImplementedError
	end

	private

	def get_system_imp(system)
		case system
		when 'polar'
			return Polar.instance
		when 'cartesian'
			return Cartesian.instance
		when 'spherical'
			return Spherical.instance
		else
			raise 'system is not support'
		end
	end
end

class Circle < Shape
	def draw(r = 2)
		@imp.draw_circle r
		p 'draw circle'
	end
end

class Rect < Shape
	def draw(left_top = {x: -1, y: 1}, right_bot = {x: 1, y: -1})
		@imp.draw_line left_top[:x], left_top[:y], right_bot[:x], left_top[:y]
		@imp.draw_line right_bot[:x], left_top[:y], right_bot[:x], right_bot[:y]
		@imp.draw_line right_bot[:x], right_bot[:y], left_top[:x], right_bot[:y]
		@imp.draw_line left_top[:x], right_bot[:y], left_top[:x], left_top[:y]
		p 'draw rect'
	end
end

n = Circle.new 'spherical'
n.draw
