require 'singleton'
require_relative 'spherical_lib'  # типа умеет рисовать линию и круг в сферических координатах,
																	# но интерфейсом отличным от ShapeImp

class ShapeImp
  include Singleton

	def draw_line(x0, y0, x1, y1)
		raise NotImplementedError
	end

	def draw_circle(r)
		raise NotImplementedError
	end

end

class Polar < ShapeImp
	def draw_line(x0, y0, x1, y1)
		p 'draw polar line'
	end

	def draw_circle(r)
		p 'draw polar circle'
	end
end

class Cartesian < ShapeImp
	def draw_line(x0, y0, x1, y1)
		p 'draw Cartesian line'
	end

	def draw_circle(r)
		p 'draw Cartesian circle'
	end
end

class Spherical < ShapeImp  # apapter client
	def initialize
		@adapter = SphericalAdapter.new
	end

	def draw_line(x0, y0, x1, y1)
		@adapter.draw_line x0, y0, x1, y1
	end

	def draw_circle(r)
		@adapter.draw_circle r
	end
end

#----------------------------------------------
class SphericalAdapter   # adapter
	def initialize
		@lib_instance = SpericalLib.new
	end

	def draw_line(x0, y0, x1, y1)
		p 'change coordinates to spherical interface'
		@lib_instance.draw_sperical_line x0 + y1, y0 + x1, x0 ** y1
	end

	def draw_circle(r)
		p 'change coordinates to spherical interface'
		@lib_instance.draw_sperical_circle 2 * r, r ** r
	end
end
#----------------------------------------------