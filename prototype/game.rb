class MapSite
	def enter
		0
	end
end

class Room < MapSite
	direction = [:north, :south, :eash, :west]

	def inicialize(room_number)
		@room_number = room_number
	end

	def set_side(direction, map_site)
	end
end

class Maze
	def add_room(room)

	end

	def room_n(room_number)
	end
end

class Wall < MapSite

end

class Door < MapSite
	def inicialize(room1, room2)
	end
end

class MazeGame

	def create_maze(factory)
		a_maze = make_maze

		r1 = make_room 1
		r2 = make_room 2
		the_door = make_door r1, r2

		a_maze.add_room r1
		a_maze.add_room r2

		r1.set_side :north, make_wall

		a_maze
	end

end

game = MazeGame.new
simpleMazeFactory = MazePrototypeFactory.new Maze.new, Wall.new, Room.new, Door.new

maze = game.create_maze simpleMazeFactory

# ---------------------

game = MazeGame.new
bombedMazeFactory = MazePrototypeFactory.new Maze.new, BombedWall.new, RoomWithABomb.new, Door.new

maze = game.create_maze bombedMazeFactory