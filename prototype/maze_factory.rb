class MazeFactory

	def inicialize
	end

	def make_maze
		Maze.new
	end

	def make_wall
		Wall.new
	end

	def make_room(room_number)
		Room.new room_number
	end

	def make_door(room1, room2)
		Door.new room1, room2
	end
end

class MazePrototypeFactory < MazeFactory

	def initialize(maze, wall, room, door)
		@prototype_maze = maze
		@prototype_wall = wall
		@prototype_room = room
		@prototype_door = door
	end

	def make_maze
	end

	def make_room(room_number)
	end

	def make_wall
		@prototype_wall.dup
	end

	def make_door(r1, r2)
		door = @prototype_door.dup
		door.initializer r1, r2
		door
	end
end