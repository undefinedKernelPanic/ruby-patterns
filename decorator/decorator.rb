class VisualComponent
	def draw
	end

	def resize
	end

end

#--------------------------------------
class TextView < VisualComponent

end
#--------------------------------------

class Decorator < VisualComponent

	def initialize(visual_component)
		@visual_component = visual_component #декорируемый объект
	end

	def draw
		@visual_component.draw
	end

	def resize
		@visual_component.resize
	end
end

class ScrollDecorator < Decorator

	# def initialize(visual_component)
		# @visual_component = visual_component #декорируемый объект
	# end

	def draw
		super
		draw_scrolls
	end

	private

	def draw_scrolls

	end
end

class BorderDecorator < Decorator

	def initialize(visual_component, width)
		@visual_component = visual_component #декорируемый объект
		@width = width
	end

	def draw
		super
		draw_border @width
	end

	private

	def draw_border(width)

	end
end

#-----------------------------------------------

def draw_window(components)
end

text_view = TextView.new

draw_window [ScrollDecorator.new(text_view)]