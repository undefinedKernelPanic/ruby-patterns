class Graphic
	def draw(at)
	end

	def handle_mouse(event)
	end

	def get_extent
	end

	def load(from)
	end

	def save(to)
	end
end

class Image < Graphic
	def initialize(file_name)
		# загрузка изображения из файла
	end

	def draw(at)
	end

	def handle_mouse(event)
	end

	def get_extent
	end

	def load(from)
	end

	def save(to)
	end
end

class Imageproxy < Graphic
	def initialize(file_name)
		@file_name = file_name
		@extent = {x: 0, y: 0} # размеры пока не известны
		@image = 0
	end

	def draw(at)
		get_image.draw at
	end

	def handle_mouse(event)
		get_image.handle_mouse event
	end

	def get_extent # возвращает кэшированные размер, если возможно
		if @extent == {x: 0, y: 0}
			@extent = get_image.get_extent
		end
		@extent
	end

	def load(from)
		from >> @extent >> @file_name
	end

	def save(to)
		to << @extent << @file_name
	end

	protected

	def get_image
		if @image == 0
			@image = # load image from file
		end
		@image
	end
end