class WindowImp
	def imp_top
		0
	end

	def imp_bottom
		0
	end

	def imp_set_extent(point)
		0
	end

	def imp_get_origin(point)
		0
	end

	def device_rect(p1, p2, p3, p4) # рисует в окне прямоугольник
		0
	end

	def device_text(text, p1, p2)
		0
	end

	def device_bitmap(text, p1, p2)
		0
	end

	protected

	def window_imp

	end
end

class XWindowlmp < WindowImp

	def initialize
		# спечифичные для платформы состояния
		@dpy = 500
		@winid = 23
		@gc = 2
	end

	def device_rect(p1, p2, p3, p4) # рисует в окне прямоугольник
		x, y, w, h = 50, 50, 55, 55
		x_draw_rectangle @dpy, @winid, @gc, x, y, w, h
	end

	# + others
end


class PMWindowImp < WindowImp

	def initialize
		# спечифичные для платформы состояния
		@hps = 3
	end

	def device_rect(p1, p2, p3, p4) # рисует в окне прямоугольник
		# какаято реализация
	end

	# + others
end