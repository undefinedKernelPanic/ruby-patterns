class Window
	def initialize(contents, imp = WindowImp.new)
		@contents = contents
		@imp = imp # абстрактный класс, в котором объявлен интерфейс к данной оконной системе
	end

	# запросы обрабатываемые окном

	def draw_contents
	end

	def open
	end

	def close
	end

	def iconify
	end

	def deiconify
	end

	# запросы, перенапрявляемы реализации

	def set_origin(at)
	end

	def set_extend(extent)
	end

	def raise
	end

	def lower
	end

	def draw_line(p1, p2)
	end

	def draw_rect(p1, p2)
		imp = get_window_imp
		imp.device_rect(p1.x, p1.y, p2.x, p2.y)
	end

	def draw_polygon(points, n)
	end

	def draw_text(text, point)
	end

	protected

	def get_window_imp
		@imp
	end

	def get_view
	end

end


class ApplicationWindow < Window
	def draw_contents
		get_view.draw_on self
	end
end

class IconWindow < Window

	def initialize(bitmap_name)
		@bitmap_name = bitmap_name
	end

	def draw_contents
		imp = get_window_imp
		if imp?
			imp.device_bitmap(@bitmap_name, 0.0, 0.0)
	end


end