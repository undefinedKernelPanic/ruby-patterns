NO_HELP_TOPIC = -1

class HelpHandler # определяет интерфейс для обработки запросов на получение справки

	def initialize(help_hendler, topic = NO_HELP_TOPIC)
		@successor = help_hendler # ссылка на приемника в цепочке обработчиков
		@topic = topic # раздел справки
	end

	def has_help
		@topic != NO_HELP_TOPIC
	end

	def set_handler(help_hendler, topic)
		@successor = help_hendler # ссылка на приемника в цепочке обработчиков
		@topic = topic # раздел справки
	end

	def handle_help # замещается в подклассах
		unless @successor.zero?
			@successor.handle_help
		end
	end
end




class Widget < HelpHandler
	def initialize(parent, topic = NO_HELP_TOPIC)
		@parent = parent
	end
end


class Button < Widget

	def initialize(parent, topic = NO_HELP_TOPIC)
		@parent = parent # виджет в котором находится
		@topic = topic # раздел справки
	end

	def handle_help
		if has_help
			'button ept'
		else
			@parent.handle_help
		end
	end

end


class Dialog < Widget

	def initialize(parent, topic = NO_HELP_TOPIC)
		@parent = parent # виджет в котором находится
		@topic = topic # раздел справки
	end

	def handle_help
		if has_help
			'dialog ept'
		else
			@parent.handle_help
		end
	end

end

class Application < HelpHandler
	def handle_help
		'application help'
	end
end



app = Application.new 0
dialog = Dialog.new app, 1
button = Button.new dialog, 4


p button.handle_help
