class Glyph

	def draw(window, glyph_context)
	end

	def set_font(font, glyph_context)

	end

	def get_font(glyph_context)

	end

	def first(glyph_context)
	end

	def next(glyph_context)
	end

	def is_done?(glyph_context)
	end

	def current(glyph_context)
	end



	def insert(glyph, glyph_context)
	end

	def remove(glyph_context)
	end

end

class Row < Glyph

end


class Character < Glyph # flyweight
	def initialize(charcode)
		@charcode = charcode # внутреннее состояние, не зависящее от контекста
	end

	def draw(window, glyph_context)

	end
end


class GlyphContext

	def initialize(fonts, index)
		# внешнее состояние
		@fonts = fonts
		@index = index
	end

	def next(step = 1)
	end

	def insert(quantity = 1)
	end

	def get_font
	end

	def set_font(fond, span = 1)
	end
end

NCHARCODES = 128

class GlyphFactory
	def initialize
		@character = Array.new NCHARCODES, 0 # содержит указатели на глифы Character, индексированные кодом символа
	end

	def create_charater(char) # ищет символ в массиве и возвращает соответствующий глиф
		unless @character.include? char
			@character[char] = new Character char
		end
		@character[char]
	end

	def create_row
		Row.new
	end

	def create_column
	end


end