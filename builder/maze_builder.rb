class MazeBuilder          # Builder

	def build_maze
	end

	def build_room(room_number)
	end

	def build_door(room_from, room_to)
	end

	def get_maze
		# return exception
	end
end


class CountingMazeBuilder < MazeBuilder       # ConcrereBullder

	def initialize
		@doors = 0
		@rooms = 0
	end

	def build_maze
		@current_maze = Maze.new
	end

	def build_room(room_number)
		@rooms += 1
	end

	def build_door(rn1, rn2)
		@doors += 1
	end

	def get_counts
		{ :doors: @doors, :rooms: @rooms }       # Product
	end

end

class StandardMazeBuilder < MazeBuilder       # ConcrereBullder
	def initializer
		@current_maze = nil
	end

	def build_maze
		@current_maze = Maze.new
	end

	def build_room(room_number)
		room = Room.new room_number
		@current_maze.add_room room

		room.set_side :north, Wall.new
	end

	def build_door(rn1, rn2)
		r1 = @current_maze.room_n rn1
		r2 = @current_maze.room_n rn2

		d = Door.new r1, r2

		r1.set_side(common_wall(r1, r2), d)
		r2.set_side(common_wall(r2, r1), d)
	end

	def get_maze
		@current_maze       # Product
	end

	private

	def common_wall(room1, room2)

	end
end