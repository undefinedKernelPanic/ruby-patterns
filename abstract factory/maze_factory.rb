class MazeFactory       # AbstractFactory

	def inicialize
	end

	def make_maze
		Maze.new
	end

	def make_wall
		Wall.new
	end

	def make_room(room_number)
		Room.new room_number
	end

	def make_door(room1, room2)
		Door.new room1, room2
	end
end

class EnchantedMazeFactory < MazeFactory       # ConcreteFactory

	def initialize
	end

	def make_room(room_number)
		EnchantedRoom.new room_number, -> {  }
	end

	def make_door(room1, room2)
		DoorNeedingSpell.new room1, room2
	end
end


class BombedMazeFactory < MazeFactory       # ConcreteFactory
	def make_wall
		BombedWall.new
	end

	def make_room(room_number)
		RoomWithBomb.new room_number
	end
end